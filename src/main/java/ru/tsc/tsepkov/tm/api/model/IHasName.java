package ru.tsc.tsepkov.tm.api.model;

import org.jetbrains.annotations.Nullable;

public interface IHasName {

    @Nullable
    String getName();

    void setName(@Nullable String name);

}

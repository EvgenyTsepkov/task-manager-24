package ru.tsc.tsepkov.tm.exception.field;

public class StatusEmptyException extends  AbstractFieldException {

    public StatusEmptyException() {
        super("Error! Status is empty...");
    }

}

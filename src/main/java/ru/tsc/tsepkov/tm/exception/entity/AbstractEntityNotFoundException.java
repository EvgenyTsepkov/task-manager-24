package ru.tsc.tsepkov.tm.exception.entity;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.tsc.tsepkov.tm.exception.AbstractException;

@NoArgsConstructor
public abstract class AbstractEntityNotFoundException extends AbstractException {

    public AbstractEntityNotFoundException(@NotNull String message) {
        super(message);
    }

    public AbstractEntityNotFoundException(@NotNull String message, @NotNull Throwable cause) {
        super(message, cause);
    }

    public AbstractEntityNotFoundException(@NotNull Throwable cause) {
        super(cause);
    }

    public AbstractEntityNotFoundException(
            @NotNull String message,
            @NotNull Throwable cause,
            final boolean enableSuppression,
            final boolean writableStackTrace
    ) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}

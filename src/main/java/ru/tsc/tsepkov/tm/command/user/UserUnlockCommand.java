package ru.tsc.tsepkov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.tsc.tsepkov.tm.enumerated.Role;
import ru.tsc.tsepkov.tm.util.TerminalUtil;

public class UserUnlockCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "unlock-user";

    @NotNull
    public static final String DESCRIPTION = "Unlock user.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[USER UNLOCK]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        serviceLocator.getUserService().unlockUserByLogin(login);
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[] {Role.ADMIN};
    }

}
